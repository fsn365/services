import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory } from '@nestjs/microservices';

export const SERVICE_NAME = 'ASSET_SERVICE';

export const ASSET_CLIENT_PROVIDER = {
  provide: SERVICE_NAME,
  inject: [ConfigService],
  useFactory: (config: ConfigService) => {
    const option = config.get('asset_service');
    return ClientProxyFactory.create(option);
  },
};

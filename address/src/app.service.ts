import { Injectable } from '@nestjs/common';
import { PgService } from './pg/pg.service';
import { RPCService } from './rpc/rpc.service';
import { AddressInfoMsgDto, AddressBalanceMsgDto } from './msg.model';

@Injectable()
export class AppService {
  constructor(private readonly pg: PgService, private rpc: RPCService) {}

  createOrUpdateAddress(msg: AddressInfoMsgDto): Promise<boolean> {
    return this.pg.createOrUpdateAddress(msg);
  }

  async updateAddressAssetBalance(msg: AddressBalanceMsgDto): Promise<boolean> {
    const { address, asset } = msg;
    const balanceData = await this.rpc.getAddressAssetBalance(address, asset);
    return this.pg.createOrUpdateAddressAssetBalance({ ...balanceData });
  }

  async updateAddressTlAssetBalance(
    msg: AddressBalanceMsgDto,
  ): Promise<boolean> {
    const { address, asset } = msg;
    const balanceData = await this.rpc.getAddressAssetTlBalance(address, asset);
    const { qty_in = -1, ...tlBalanceData } = balanceData;
    const promises: any[] = [];
    if (qty_in !== -1) {
      const updateQtyIn = this.pg.createOrUpdateAddressAssetBalance({
        address,
        asset,
        qty_in,
      });
      promises.push(updateQtyIn);
    }
    const updateTlbalance = this.pg.createOrUpdateAddressTlBalance({
      ...tlBalanceData,
    });
    promises.push(updateTlbalance);

    return Promise.all(promises)
      .then(result => {
        const [blRes, tlRes] = result;
        if (blRes && tlRes) {
          return true;
        }
        return false;
      })
      .catch(() => false);
  }
}

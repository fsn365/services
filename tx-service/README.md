# Tx services

## Functionality

- Decode none *BuyTicketFunc* transactions.

## Message

Please refer [app.controller.ts](./src/app.controller.ts);

Tables in PostgreSQL

## Related tables

```SQL
--- tracked erc20 tokens --- tracked erc20 tokens 
drop table if exists erc20;
create table erc20 (
  id            serial      primary key,
  hash          char(42)    unique not null,
  name          text        not null,
  symbol        text        not null,
  precision     int         not null,
  qty           text        default 0,
  exchange      char(42)    not null
);

-- mbtc
drop table if exists erc20_mbtc_txs;
create table erc20_mbtc_txs (
  id          bigserial     primary key,
  hash        char(66)      unique not null,
  timestamp   int           not null,
  block       int           not null,
  sender      char(42)      not null,
  receiver    char(42)      not null,
  fee         text          not null,
  status      int           not null,
  data        json          not null
);
drop table if exists erc20_mbtc_holders;
create table erc20_mbtc_holders (
  id         bigserial    primary key,
  hash       char(42)     unique not null,
  qty        numeric         not null
);



--- any token
drop table if exists erc20_any_txs;
create table erc20_any_txs(
  id          bigserial     primary key,
  hash        char(66)      unique not null,
  timestamp   int           not null,
  block       int           not null,
  sender      char(42)      not null,
  receiver    char(42)      not null,
  fee         text          not null,
  status      int           not null,
  data        json          not null
);

drop table if exists erc20_any_holders;
create table erc20_any_holders (
  id        bigserial     primary key,
  holder    char(42)      unique not null,
  qty       numeric       not null
);


-- meth
drop table if exists erc20_meth_txs;
create table erc20_meth_txs(
  id          bigserial     primary key,
  hash        char(66)      unique not null,
  timestamp   int           not null,
  block       int           not null,
  sender      char(42)      not null,
  receiver    char(42)      not null,
  fee         text          not null,
  status      int           not null,
  data        json          not null
);
drop table if exists erc20_meth_holders;
create table erc20_meth_holders (
  id         bigserial    primary key,
  hash       char(42)     unique not null,
  qty        numeric         not null
);

--musdt
drop table if exists erc20_musdt_txs;
create table erc20_musdt_txs(
  id          bigserial     primary key,
  hash        char(66)      unique not null,
  timestamp   int           not null,
  block       int           not null,
  sender      char(42)      not null,
  receiver    char(42)      not null,
  fee         text          not null,
  status      int           not null,
  data        json          not null
);
drop table if exists erc20_musdt_holders;
create table erc20_musdt_holders (
  id         bigserial    primary key,
  hash       char(42)     unique not null,
  qty        numeric         not null
);
```
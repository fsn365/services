import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller('app')
export class AppController {
  constructor(private service: AppService) {}

  @MessagePattern('balance')
  async updateAddressBalance(
    @Payload() msg: { address: string; token: string },
  ) {
    this.updateAddressBalance(msg);
  }

  @MessagePattern('txs')
  async getTokenTxs(@Payload() contract: string): Promise<any[]> {
    return this.getTokenTxs(contract);
  }

  @MessagePattern('holders')
  async getTokenHolders(@Payload() contract: string): Promise<any[]> {
    return this.getTokenHolders(contract);
  }

  @MessagePattern('info')
  async getTokenInfo(@Payload() contract: string): Promise<any> {
    return this.service.getTokensInfo(contract);
  }
}

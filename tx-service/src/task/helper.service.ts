import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { from as rxJsFrom } from 'rxjs';
import { RedisService } from 'nestjs-redis';
import { Interval } from '@nestjs/schedule';

import { AssetService } from './asset/asset.service';
import { RPCService } from './rpc.service';
import { AddressService } from './address/address.service';

import {
  TRANSACTION_TYPES,
  SWAP_TYPES,
  FSN_CONTRACT,
  FSN_TOKEN,
} from './constants';

import { AssetBalanceMsg, FsnBalanceMsg } from './msg.model';
import { ProcessedTx, JsonlizedTx, RawTx } from 'src/tx.model';

const Web3 = require('web3');
const web3FusionExtend = require('web3-fusion-extend');

@Injectable()
export class HelperService {
  // for erc20 token decoding usage
  private web3: any = null;
  private redis: any = null;
  private ERC20_TOKENS = null;

  constructor(
    private config: ConfigService,
    private asset: AssetService,
    private address: AddressService,
    private rpc: RPCService,
    redisService: RedisService,
  ) {
    const RPC_PROVIDER = this.config.get('rpc_url');
    this.web3 = web3FusionExtend.extend(new Web3(RPC_PROVIDER));
    this.redis = redisService.getClient('local');
    this.ERC20_TOKENS = this.config.get('erc20');
  }

  // process transactions
  async processTxs(
    txsData: RawTx[],
  ): Promise<{ txs: JsonlizedTx[]; ok: boolean }> {
    const promises = txsData.map(tx => {
      const txData = this.calcTxFee(tx);
      return this.processTx(txData);
    });
    return Promise.all(promises)
      .then(data => {
        const txs = data.map(txItem => {
          delete txItem.ivalue;
          delete txItem.dvalue;
          delete txItem.erc20Receipts;
          return this.jsonlizeTxData(txItem);
        });
        return { txs, ok: true };
      })
      .catch(e => {
        console.log(`Process transactions failed.`);
        console.log(JSON.stringify(e));
        return { ok: false, txs: [] };
      });
  }

  // process transaction based on transaction type
  private async processTx(txData: RawTx): Promise<ProcessedTx> {
    const type = txData.type;

    switch (type) {
      case 'SendAssetFunc':
        return this.processSendAssetTx(txData);
      case 'TimeLockFunc':
        return this.processTimeLockTx(txData);
      case 'MakeSwapFunc':
      case 'RecallSwapFunc':
      case 'TakeSwapFunc':
      case 'MakeSwapFuncExt':
      case 'TakeSwapFuncExt':
      case 'MakeMultiSwapFunc':
      case 'RecallMultiSwapFunc':
      case 'TakeMultiSwapFunc':
        return this.processSwapTx(txData);
      case 'GenAssetFunc':
        return this.processGenAssetTx(txData);
      case 'AssetValueChangeFunc':
        return this.processAssetChangeTx(txData);
      case 'GenNotationFunc':
        return this.processGenNotationTx(txData);
      case 'Origin':
        return this.processOriginTx(txData);
      default:
        return this.processOtherTypesTx(txData);
    }
  }

  // calculate transaction's fee
  private calcTxFee(tx: RawTx) {
    const { gasLimit, gasPrice, ...others } = tx;
    const fee = (+gasLimit * +gasPrice) / Math.pow(10, 18);
    return { ...others, fee: fee + '' };
  }

  // process send asset transaction
  private async processSendAssetTx(txData: any): Promise<ProcessedTx> {
    const { type, from, to, log, timestamp, ...tx } = txData;

    if (TRANSACTION_TYPES[type] !== 2) return txData;

    tx.type = TRANSACTION_TYPES[type];

    const { AssetID, Value, To } = log;

    this.updateFsnBalance({ address: from, timestamp });
    if (AssetID !== FSN_TOKEN) {
      this.updateBalance({ address: from, asset: AssetID, timestamp });
    }
    this.updateBalance({ address: To, asset: AssetID, timestamp });

    const { symbol, precision } = await this.getAssetInfo(AssetID);
    const value = Value / Math.pow(10, precision);
    const data = { value, symbol, asset: AssetID };

    const assets = [AssetID];

    return { ...tx, sender: from, receiver: To, assets, data, timestamp };
  }

  // process time lock transaction
  private async processTimeLockTx(txData: any): Promise<ProcessedTx> {
    const { type, from, to, log, timestamp, ...tx } = txData;

    if (TRANSACTION_TYPES[type] !== 3) return txData;

    tx.type = TRANSACTION_TYPES[type];

    const { AssetID, Value, StartTime, EndTime, LockType, To, Type } = log;

    this.updateFsnBalance({ address: from, timestamp });
    this.updateTlBalance({ address: from, asset: AssetID, timestamp });
    this.updateTlBalance({ address: To, asset: AssetID, timestamp });

    const { precision, symbol } = await this.asset.getAssetSnapshot(AssetID);
    const value = Value / Math.pow(10, precision);
    const data = {
      value,
      asset: AssetID,
      symbol,
      startTime: StartTime,
      endTime: EndTime,
      type: Type,
      lockType: LockType,
    };
    const assets = [AssetID];

    return { ...tx, sender: from, receiver: To, assets, data, timestamp };
  }

  // process asset generation transaction
  private async processGenAssetTx(txData: any): Promise<ProcessedTx> {
    const { type, from, to, log, timestamp, ...tx } = txData;

    if (TRANSACTION_TYPES[type] !== 1) return txData;

    tx.type = TRANSACTION_TYPES[type];

    const { AssetID, Total, Decimals, Symbol } = log;

    this.updateFsnBalance({ address: from, timestamp });
    this.updateBalance({ address: from, asset: AssetID, timestamp });

    const assets = [AssetID];

    const value = Total / Math.pow(10, Decimals);
    const data = { symbol: Symbol, value, asset: AssetID };
    const receiver = FSN_CONTRACT;

    return { ...tx, sender: from, receiver, assets, data, timestamp };
  }

  // process USAN generation transaction
  private async processGenNotationTx(txData: any): Promise<ProcessedTx> {
    const { type, from, to, log, timestamp, ...tx } = txData;

    if (TRANSACTION_TYPES[type] !== 0) return txData;

    tx.type = TRANSACTION_TYPES[type];

    this.updateFsnBalance({ address: from, timestamp, usan: log.Notation });

    const data = { usan: log.Notation };

    return { ...tx, sender: from, receiver: FSN_CONTRACT, data, timestamp };
  }

  // process asset issue quantity change transaction
  private async processAssetChangeTx(txData: any): Promise<ProcessedTx> {
    const { type, from, to, log, timestamp, ...tx } = txData;

    if (TRANSACTION_TYPES[type] !== 12) return txData;
    tx.type = TRANSACTION_TYPES[type];

    const { IsInc, AssetID, Value } = log;

    this.updateFsnBalance({ address: from, timestamp });
    this.updateBalance({ address: from, asset: AssetID, timestamp });

    const assets = [AssetID];

    const { precision, symbol } = await this.asset.getAssetSnapshot(AssetID);
    const data = {
      isInc: IsInc,
      value: Value / Math.pow(10, precision),
      symbol,
      asset: AssetID,
    };

    const receiver = FSN_CONTRACT;

    return { ...tx, sender: from, receiver, assets, data, timestamp };
  }

  // process swap related transactions
  private async processSwapTx(txData: any): Promise<ProcessedTx> {
    const { type, to, from, log, timestamp, ...tx } = txData;
    if (!SWAP_TYPES.includes(type)) return txData;

    tx.type = TRANSACTION_TYPES[type];

    const assets = await this.getRelatedAssetsForSwapTx(txData);

    rxJsFrom(assets).subscribe((asset: string) => {
      // As we are not sure the swaped asset is a normal one or a timelock one,
      // we update both types here.
      this.updateBalance({ address: from, asset, timestamp });
      this.updateTlBalance({ address: from, asset, timestamp });
    });

    const data = { swap: log.SWapID };

    const receiver = FSN_CONTRACT;

    return { ...tx, sender: from, receiver, assets, data, timestamp };
  }

  // EmptyFunc, ReportIllegalFunc, etc.
  private async processOtherTypesTx(txData: any): Promise<ProcessedTx> {
    const { from, to, log, timestamp, type, ...tx } = txData;

    //Set unkown transaction type as -1
    tx.type = TRANSACTION_TYPES[type] || -1;

    this.updateFsnBalance({ address: from, timestamp });

    const receiver = (log && log.To) || to || FSN_CONTRACT;

    return { ...tx, sender: from, receiver, data: log, timestamp };
  }

  // smart contract related transaction or pure origin  transaction
  private async processOriginTx(txData: any): Promise<ProcessedTx> {
    const { type, from, to, log, ivalue, dvalue, timestamp, ...tx } = txData;
    if (TRANSACTION_TYPES[type] !== 17) return txData;

    // pure origin transaction
    if (log === null) {
      this.updateFsnBalance({ address: from, timestamp });
      this.updateFsnBalance({ address: to, timestamp });

      tx.type = TRANSACTION_TYPES['SendAssetFunc'];

      const value = +ivalue + +dvalue / Math.pow(10, 18);
      const data = { value, symbol: FSN_TOKEN, asset: FSN_TOKEN };

      const assets = [FSN_TOKEN];

      return { ...tx, sender: from, receiver: to, data, assets, timestamp };
    }

    // contract related transactions
    let data: any = {},
      assets: string[] = [];
    const contracts: string[] = [];
    log.map((logItem: any) => {
      const { contract, topics = [] } = logItem;
      if (contract) contracts.push(contract);
    });

    return {
      ...tx,
      sender: from,
      receiver: to,
      data,
      assets,
      timestamp,
    };
  }

  async processERC20Tx(txData: any) {
    const { type, erc20Receipts = [], ...tx } = txData;
    if (type !== 'ERC20' || erc20Receipts.length === 0) return txData;

    const { from, to, contract } = erc20Receipts[0];
    const token = this.getErc20Token(contract);
    const assets = [contract];

    const data = { ...token };
  }

  private async getRelatedAssetsForSwapTx(txData: any): Promise<string[]> {
    const { type, log } = txData;
    let assets: string[] = [];
    if (type.indexOf('Take') > -1 || type.indexOf('Recall') > -1) {
      const { FromAssetID, ToAssetID } = await this.rpc.getSwapInfo(
        log.SwapID,
        type,
      );
      assets = Array.from(new Set([...FromAssetID, ...ToAssetID, FSN_TOKEN]));
      return assets;
    }
    if (type === 'MakeMultiSwapFunc') {
      const { FromAssetID, ToAssetID } = log;
      assets = Array.from(new Set([...FromAssetID, ...ToAssetID, FSN_TOKEN]));
      return assets;
    }
    if (type === 'MakeSwapFuncExt' || type === 'MakeSwapFunc') {
      const { FromAssetID, ToAssetID } = log;
      const assets = Array.from(new Set([FromAssetID, ToAssetID, FSN_TOKEN]));
      return assets;
    }
    return assets;
  }

  cacheValue(key: string, value: any) {
    this.redis.set(key, JSON.stringify(value));
  }

  getCachedValue(key: string): Promise<string> {
    return this.redis.get(key);
  }

  getLatestBlockHeight(): Promise<number> {
    return this.getCachedValue('l:bk').then((val: string) => {
      if (val) return +val;
      return -1;
    });
  }

  private jsonlizeTxData(txData: any) {
    const { assets, data, ...tx } = txData;
    if (assets) {
      tx.assets = JSON.stringify(assets);
    }
    if (data) {
      tx.data = JSON.stringify(data);
    }
    return tx;
  }

  private getAssetInfo(
    assetID: string,
  ): Promise<{ precision: number; symbol: string }> {
    return this.asset.getAssetSnapshot(assetID);
  }

  private updateTlBalance(msg: AssetBalanceMsg) {
    this.address.updateAddressTimeLockBalance(msg);
  }

  private updateBalance(msg: AssetBalanceMsg) {
    this.address.updateAddressBalance(msg);
  }

  private updateFsnBalance(msg: FsnBalanceMsg) {
    this.address.updateAddressFsnBalance(msg);
  }

  private getErc20Token(tokenContract: string) {
    const ERC20_TOKENS = this.ERC20_TOKENS;
    return ERC20_TOKENS[tokenContract];
  }

  @Interval(4000)
  private trackLatestBlockHeight() {
    this.web3.eth.getBlockNumber((error: any, result: number) => {
      if (error) {
        console.log(error);
      }
      if (result) {
        this.cacheValue('l:bk', result);
      }
    });
  }
}

import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';
import { BlockQueryDto, IPagedBlock, IBlock } from './models/block.model';

@Controller()
export class AppController {
  constructor(private readonly service: AppService) {}

  @MessagePattern('blocks')
  getBlocks(@Payload() query: BlockQueryDto): Promise<IPagedBlock[]> {
    return this.service.getBlocks(query);
  }

  @MessagePattern('block')
  getBlock(@Payload() height: number): Promise<IBlock> {
    return this.service.getBlock(height);
  }

  @MessagePattern('lbk')
  getLBk(): Promise<number> {
    return this.service.getLBK();
  }
}

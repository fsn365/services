export class FsnBalanceMsg {
  address: string;
  usan?: number;
  timestamp: number;
}

export interface AssetBalanceMsg extends FsnBalanceMsg {
  asset: string;
}

export class AddressInfoMsg {
  address: string;
  usan?: number;
  timestamp: number;
}

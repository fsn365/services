import { Controller, Logger } from '@nestjs/common';
import {
  Ctx,
  Payload,
  RmqContext,
  MessagePattern,
} from '@nestjs/microservices';
import { AppService } from './app.service';
import { AddressBalanceMsgDto, AddressInfoMsgDto } from './msg.model';

@Controller()
export class AppController {
  private logger = new Logger(`AppController`);
  constructor(private readonly appService: AppService) {}

  @MessagePattern('address')
  async createOrUpdateAddress(
    @Payload() msg: AddressInfoMsgDto,
    @Ctx() ctx: RmqContext,
  ) {
    const isDone = await this.appService.createOrUpdateAddress(msg);
    if (isDone) this.ackMsg(ctx);
  }

  @MessagePattern('address:balance')
  async updateAddressBalance(
    @Payload() msg: AddressBalanceMsgDto,
    @Ctx() ctx: RmqContext,
  ) {
    const isDone = await this.appService.updateAddressAssetBalance(msg);
    if (isDone) this.ackMsg(ctx);
  }

  @MessagePattern('address:tl_balance')
  async updateAddressTlBalance(
    @Payload() msg: AddressBalanceMsgDto,
    @Ctx() ctx: RmqContext,
  ) {
    const isDone = await this.appService.updateAddressTlAssetBalance(msg);
    if (isDone) this.ackMsg(ctx);
  }

  private ackMsg(ctx: RmqContext) {
    const rawMsg = ctx.getMessage();
    const channel = ctx.getChannelRef();
    channel.ack(rawMsg);

    const msg = JSON.parse(rawMsg.content.toString());
    this.logger.log(`Ack ${msg.pattern}, data:`);
    this.logger.log(msg.data);
  }
}

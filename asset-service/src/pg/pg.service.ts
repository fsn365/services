import { Injectable, Logger } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { IUpdateAsset } from '../asset.model';

@Injectable()
export class PgService {
  private readonly logger = new Logger('Asset:PgService');
  constructor(@InjectKnex() private readonly knex: Knex) {}

  async createOrUpdateAssets(assets: IUpdateAsset[]) {
    const promises = assets.map(async (asset: IUpdateAsset) =>
      this.createOrUpdateAsset(asset),
    );
    return Promise.all(promises)
      .then(() => {
        this.logger.log(`Updated ${assets.length} assets.`);
        return true;
      })
      .catch(e => {
        this.logger.log(`Update ${assets.length} assets failed.`);
        console.log(JSON.stringify(e));
        return false;
      });
  }

  // create an asset or update asset's issue quantity
  private async createOrUpdateAsset(asset: IUpdateAsset) {
    return this.knex.transaction(trx => {
      return trx
        .insert(asset)
        .into('assets')
        .then(() => {
          this.logger.log(`Created asset ${asset.hash}.`);
        })
        .catch(e => {
          this.logger.log(`Insert asset failed. Will try updating asset.`);
          const update: IUpdateAsset = { hash: asset.hash, qty: asset.qty };
          return this.updateAsset(update);
        });
    });
  }

  private async updateAsset(asset: IUpdateAsset) {
    return this.knex.transaction(trx => {
      const { hash, ...update } = asset;
      return trx
        .update(update)
        .where({ hash })
        .from('assets')
        .then(() => {
          this.logger.log(`updated asset ${asset.hash}.`);
          return true;
        })
        .catch(e => {
          this.logger.log(`Update assert ${asset.hash} failed.`);
          this.logger.log(e);
          return false;
        });
    });
  }
}

import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './asset.client.provider';

@Injectable()
export class AssetService {
  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  onApplicationBootstrap() {
    this.getAssetSnapshot(
      '0x6a102b41bb79570e962309e5bc7137606a817b446326b6d0c54e28e4e498c133',
    ).subscribe(data => {
      console.log(data);
    });
  }
  getAssetSnapshot(assetHash: string) {
    return this.client.send('snapshot', assetHash);
  }
}

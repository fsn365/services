import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';
import { IAssetSnapshot } from './asset.model';

@Controller()
export class AppController {
  constructor(private readonly service: AppService) {}

  @MessagePattern('snapshot')
  async getAssetSnapshot(@Payload() asset: string): Promise<IAssetSnapshot> {
    return this.service.getAssetSnapshot(asset);
  }
}

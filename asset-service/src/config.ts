import { Transport } from '@nestjs/microservices';

export const config = () => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  redis: {
    name: 'asset',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
  },
  rpc: process.env.rpc_url,
  app: {
    name: 'Asset Service',
    transport: Transport.TCP,
    options: {
      port: process.env.app_port || 8899,
      host: process.env.app_host || '127.0.0.1',
    },
  },
});

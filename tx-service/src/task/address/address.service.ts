import { Injectable, Inject, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './address.client.provider';
import { FSN_TOKEN } from '../constants';
import { AssetBalanceMsg, FsnBalanceMsg, AddressInfoMsg } from '../msg.model';

@Injectable()
export class AddressService {
  private logger = new Logger('Tx: AddressService');
  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  updateAddressBalance(msg: AssetBalanceMsg) {
    const { address, asset, timestamp, usan } = msg;
    this.client.emit('address:balance', { address, asset });
    this.updateAddressInfo({ address, timestamp, usan });
  }

  updateAddressFsnBalance(msg: FsnBalanceMsg) {
    this.updateAddressBalance({ ...msg, asset: FSN_TOKEN });
  }

  updateAddressTimeLockBalance(msg: AssetBalanceMsg) {
    const { address, asset, timestamp } = msg;
    this.client.emit('address:tl_balance', { address, asset });
    this.updateAddressInfo({ address, timestamp });
  }

  updateAddressInfo(msg: AddressInfoMsg) {
    const msgData = { ...msg };
    if (!msgData.usan) delete msgData.usan;
    this.client.emit('address', msgData);
  }
}

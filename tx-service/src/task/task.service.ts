import { Injectable, Logger } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { PgService } from '../pg/pg.service';
import { MongoService } from '../mongo/mongo.service';
import { HelperService } from './helper.service';

@Injectable()
export class TaskService {
  private logger = new Logger('Tx: TaskService');
  constructor(
    private readonly pg: PgService,
    private readonly mongo: MongoService,
    private readonly helper: HelperService,
  ) {}

  async onApplicationBootstrap() {
    const prevTrackAt = await this.pg.findPrevTrackTx();
    this.setPrevtrackAt(prevTrackAt);
  }

  @Interval(5000)
  async handleInterval() {
    const range = await this.getRange();
    const rawTxs = await this.mongo.fetchTxs(range);
    const { txs, ok } = await this.helper.processTxs(rawTxs);
    if (ok) {
      await this.pg.insertTxsToDB(txs).then(id => {
        this.helper.cacheValue('tx_count', id);
        this.setPrevtrackAt(range.$lte);
      });
    }
  }

  async getRange(): Promise<{ $lte: number; $gt: number }> {
    const step = 100;
    const prevTrackAt = await this.getPrevTrackAt();
    const lBk = await this.helper.getLatestBlockHeight();
    return Promise.all([prevTrackAt, lBk])
      .then(data => {
        const [$gt, height] = data;
        const $lte = Math.min($gt + step, height);
        return { $lte, $gt };
      })
      .catch(e => ({ $lte: 0, $gt: 0 }));
  }

  private setPrevtrackAt(value: number) {
    this.helper.cacheValue('tx:track_at', value);
  }

  private getPrevTrackAt() {
    return this.helper.getCachedValue('tx:track_at').then(val => {
      if (val) return +val;
      return -1;
    });
  }
}

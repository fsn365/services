import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { config } from './config';
import { AppController } from './app.controller';
import { HelperModule } from './helper/helper.module';
import { PgModule } from './pg/pg.module';
import { ExchangeModule } from './exchange/exchange.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    HelperModule,
    PgModule,
    ExchangeModule,
  ],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}

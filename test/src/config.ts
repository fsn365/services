import { Transport } from '@nestjs/microservices';

export const config = () => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },
  rabbitmq: {
    urls: [process.env.rabbitmq_url],
    queue: process.env.rabbitmq_queue,
    prefetchCount: 1,
    noAck: false,
    queueOptions: {
      durable: true,
    },
  },
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  redis: {
    name: 'local',
    host: 'localhost',
    port: 6379,
    ttl: 4,
  },
  rpc_url: process.env.rpc_url,
  address_service: {
    name: 'Address Service',
    transport: Transport.RMQ,
    options: {
      urls: [process.env.rabbitmq_url],
      queue: process.env.rabbitmq_queue,
      prefetchCount: 1,
      noAck: false,
      queueOptions: {
        durable: true,
      },
    },
  },
  block_service: {
    name: 'Block Services',
    transport: Transport.TCP,
    options: {
      host: process.env.app_host || '127.0.0.1',
      port: 8888,
    },
  },
  asset_service: {
    name: 'Asset Service',
    transport: Transport.TCP,
    options: {
      host: process.env.asset_service_host || '127.0.0.1',
      port: +process.env.asset_service_port || 8889,
    },
  },
});

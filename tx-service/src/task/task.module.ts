import { Module, HttpModule } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { RedisModule } from 'nestjs-redis';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PgModule } from '../pg/pg.module';
import { MongoModule } from '../mongo/mongo.module';
import { HelperService } from './helper.service';
import { TaskService } from './task.service';
import { AssetModule } from './asset/asset.module';
import { AddressModule } from './address/address.module';
import { RPCService } from './rpc.service';

@Module({
  imports: [
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => config.get('redis'),
    }),
    HttpModule.register({}),
    ScheduleModule.forRoot(),
    MongoModule,
    AssetModule,
    AddressModule,
    PgModule,
  ],
  providers: [TaskService, HelperService, RPCService],
  exports: [HelperService],
})
export class TaskModule {}

import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import TxDoc from './transaction.interface';
import { RawTx } from '../tx.model';

@Injectable()
export class MongoService {
  private logger = new Logger(`Tx: MongoService`);
  constructor(
    @InjectModel('Transactions') private readonly model: Model<TxDoc>,
  ) {}

  async fetchTxs(range: { $lte: number; $gt: number }): Promise<RawTx[]> {
    this.logger.log(`Fetch none BuyTicket transactions...`);

    const { $lte, $gt } = range;
    const $limit = $lte - $gt;
    if ($limit === 0) return [];
    const $match = { type: { $ne: 'BuyTicketFunc' }, blockNumber: range };

    return this.model
      .aggregate([
        { $match },
        {
          $project: {
            _id: 0,
            hash: 1,
            type: 1,
            gasLimit: 1,
            gasPrice: 1,
            status: 1,
            from: 1,
            to: 1,
            timestamp: 1,
            log: 1,
            ivalue: 1,
            dvalue: 1,
            erc20Receipts: 1,
          },
        },
        { $sort: { block: -1 } },
      ])
      .then(data => {
        this.logger.log(`Found ${data.length} transactions`);
        return data;
      })
      .catch(e => {
        this.logger.log(`Fetch transactions from database failed`);
        this.logger.log(JSON.stringify(e));
        return [];
      });
  }
}

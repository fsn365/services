export interface AddressBalanceMsg {
  address: string;
  token: string;
}

export interface TokenHolderInfo {
  holder: string;
  token: string;
  qty: number;
}

export interface CreateTokenHolderInfo extends TokenHolderInfo {
  table: string;
}

export interface TokenTableConfig {
  holders: string;
  txs: string;
}

// Dont handle data process or validation here
// as the API server takes care of validation
export class QueryPagedDataDto {
  page: number;
  size: number;
}

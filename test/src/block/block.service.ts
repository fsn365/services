import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './block.client.provider';

@Injectable()
export class BlockService {
  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  async onApplicationBootstrap() {
    await this.client.connect();
    (await this.getLBk())
      .toPromise()
      .then(data => {
        console.log(data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  async getBlocks() {
    const msg = { page: 1000, size: 100 };
    return this.client.send('blocks', msg);
  }

  async getBlock() {
    return this.client.send('block', 10);
  }

  async getLBk() {
    return this.client.send('lbk', '');
  }
}

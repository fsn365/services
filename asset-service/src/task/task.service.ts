import { Injectable, Logger } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { PgService } from '../pg/pg.service';
import { MongoService } from '../mongo/mongo.service';
import { HelperService } from './helper.service';

@Injectable()
export class TaskService {
  private logger = new Logger('Asset: TaskService');
  constructor(
    private readonly pg: PgService,
    private readonly mongo: MongoService,
    private readonly helper: HelperService,
  ) {}

  @Interval(4000)
  private async handleInterval() {
    const prevTrackAt = await this.helper.getPrevTrackAt();
    const { assets, syncBk } = await this.mongo.AggregateAssets(prevTrackAt);
    if (assets.length) {
      this.logger.log(`Found ${assets.length} assets to update.`);
      await this.helper
        .updateAssetsQty(assets)
        .then(assets => this.pg.createOrUpdateAssets(assets));
    } else {
      this.logger.log(`Found 0 asset to update.`);
    }
    this.helper.setPrevTrackAt(syncBk);
  }
}

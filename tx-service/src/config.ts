import { Transport } from '@nestjs/microservices';

export const config = () => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  redis: {
    name: 'local',
    host: 'localhost',
    port: 6379,
    ttl: 4,
  },
  address_service: {
    name: 'Address Service',
    transport: Transport.RMQ,
    options: {
      urls: [process.env.rabbitmq_url],
      queue: process.env.rabbitmq_queue,
      prefetchCount: 1,
      noAck: false,
      queueOptions: {
        durable: true,
      },
    },
  },
  asset_service: {
    name: 'Asset Service',
    transport: Transport.TCP,
  },
  app: {
    name: 'Tx Service',
    transport: Transport.TCP,
    options: {
      port: process.env.app_port || 8899,
      host: process.env.app_host || '127.0.0.1',
    },
  },
  erc20: {
    btc: {
      contract: process.env.erc20_btc,
      exchange: process.env.erc20_btc_exchange,
      symbol: 'mBTC',
      name: 'SMPC Bitcoin',
      precision: 8,
    },
    any: {
      contract: process.env.erc20_any,
      exchange: process.env.erc20_any_exchange,
      symbol: 'ANY',
      name: 'Anyswap',
      precision: 18,
    },
    eth: {
      contract: process.env.erc20_eth,
      exchange: process.env.erc20_eth_exchange,
      name: 'Ethereum',
      symbol: 'mETH',
      precision: 18,
    },
    usd: {
      contract: process.env.erc20_usd,
      exchange: process.env.erc20_usd_exchange,
      name: 'Tether',
      symbol: 'mUSDT',
      precision: 6,
    },
  },
});

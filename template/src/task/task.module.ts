import { Module, HttpModule } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { RedisModule } from 'nestjs-redis';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TaskService } from './task.service';
import { PgModule } from '../pg/pg.module';
import { MongoModule } from '../mongo/mongo.module';
import { TaskRedisService } from './task-redis.service';
import { TaskHelperService } from './task-helper.service';

@Module({
  imports: [
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => config.get('redis'),
    }),
    HttpModule.register({}),
    ScheduleModule.forRoot(),
    MongoModule,
    PgModule,
  ],
  providers: [TaskService, TaskRedisService, TaskHelperService],
})
export class TaskModule {}

import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { config } from './config';

async function bootstrap() {
  const APP_CONFIG = config();
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    port: APP_CONFIG.app.port
  });

  await app.listen(() => {
    console.log(`${APP_CONFIG.app.name} services started...`);
  });
}
bootstrap();

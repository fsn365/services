export interface TokenTableConfig {
  holders: string;
  txs: string;
}

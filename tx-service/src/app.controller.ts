import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly service: AppService) {}

  @MessagePattern('progress')
  async getTxDecodingProgress(): Promise<number> {
    return this.service.getTxDecodingProgress();
  }
}

export interface IAssetSnapshot {
  symbol: string;
  precision: string;
}

export interface IAsset extends IAssetSnapshot {
  hash: string;
  name: string;
  qty: string;
  issuer: string;
  issue_time: number;
  issue_tx: string;
  canchange: boolean;
  description?: any;
}

export interface IUpdateAsset extends Partial<IAsset> {}

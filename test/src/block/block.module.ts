import { Module } from '@nestjs/common';
import { BlockService } from './block.service';
import { BLOCK_CLIENT_PROVIDER } from './block.client.provider';

@Module({
  providers: [BLOCK_CLIENT_PROVIDER, BlockService],
  exports: [BlockService],
})
export class BlockModule {}

export class AddressBalanceMsgDto {
  address: string;
  asset: string;
}

export class AddressInfoMsgDto {
  address: string;
  miner?: boolean;
  contract?: boolean;
  timestamp: number;
  usan?: string | number;
}

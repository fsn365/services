import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TokenTableConfig } from '../token.model';

@Injectable()
export class ExchangeService {
  private ERC20_TOKENS: any;

  constructor(private readonly config: ConfigService) {
    this.ERC20_TOKENS = this.config.get('erc20');
  }

  getTokenTableConfig(tokenContract: string): TokenTableConfig {
    const token = this.ERC20_TOKENS[tokenContract];

    if (!token) return null;

    const tokenSymbol = token.symbol;

    switch (tokenSymbol) {
      case 'mBTC':
        return { holders: 'erc20_mbtc_holders', txs: 'erc20_mbtc_txs' };
      case 'ANY':
        return { holders: 'erc20_any_holders', txs: 'erc20_any_txs' };
      case 'mETH':
        return { holders: 'erc20_meth_holders', txs: 'erc20_meth_txs' };
      case 'mUSDT':
        return { holders: 'erc20_musdt_holders', txs: 'erc20_musdt_txs' };
      default:
        return null;
    }
  }
}

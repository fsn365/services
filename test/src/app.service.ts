import { Injectable, Get, Param } from '@nestjs/common';
import { AssetService } from './asset/asset.service';

@Injectable()
export class AppService {
  constructor(private asset: AssetService) {}

  @Get()
  getAssetSnapshot() {
    return this.asset.getAssetSnapshot(
      '0x6a102b41bb79570e962309e5bc7137606a817b446326b6d0c54e28e4e498c133',
    );
  }
}

import { Injectable, Logger } from '@nestjs/common';
import {
  AddressBalanceMsg,
  CreateTokenHolderInfo,
  QueryPagedDataDto,
} from './token.model';
import { HelperService } from './helper/helper.service';
import { PgService } from './pg/pg.service';
import { ExchangeService } from './exchange/exchange.service';

@Injectable()
export class AppService {
  private logger = new Logger('Erc20Servie:AppService');
  constructor(
    private helper: HelperService,
    private pg: PgService,
    private exchange: ExchangeService,
  ) {}

  getTokensTxs(contract: string, query: QueryPagedDataDto): Promise<any[]> {
    this.logger.log(`Get transactions for erc20 token: ${contract}.`);
    return Promise.resolve([]);
  }

  getTokensHolders(contract: string, query: QueryPagedDataDto): Promise<any[]> {
    this.logger.log(`Get holders for erc20 token:${contract}.`);
    return Promise.resolve([]);
  }

  getTokensInfo(contract: string): Promise<any> {
    this.logger.log(`Get token:${contract} info.`);
    return Promise.resolve({});
  }

  async updateAddressTokenBalance(msg: AddressBalanceMsg) {
    const { token, address } = msg;
    const tokenTableConfig = this.exchange.getTokenTableConfig(token);
    // If there is no configure for this token, the request is a bad data. So we ack it directly.
    if (!tokenTableConfig) return true;

    const { holders } = tokenTableConfig;
    const { qty } = await this.helper.getAddressTokenBalance(address, token);
    const tokenHolderData: CreateTokenHolderInfo = {
      table: holders,
      holder: address,
      token,
      qty,
    };

    return this.pg.createOrUpdateAddressTokenBalance(tokenHolderData);
  }
}

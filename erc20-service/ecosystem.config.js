const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

module.exports = {
  apps : [{
    name: 'erc20 service',
    script: 'dist/main.ts',
    instances: 'max',
    autorestart: true,
    watch: false,
    max_memory_restart: '512M',
  }]
};

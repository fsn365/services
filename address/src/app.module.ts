import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { PgModule } from './pg/pg.module';
import { config } from './config';
import { RpcModule } from './rpc/rpc.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    HttpModule.register({
      timeout: 5000,
    }),
    PgModule,
    RpcModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

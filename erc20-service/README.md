# Erc20 services


## Functionalities
- Track erc20 token's transactions
- Provide API services for erc20 tokens 
  - provide transaction list for an erc20 token
  - provide holder list for an erc20 token
  - provide token information for an erc20 token


##  The exposed API

Please refer [app.controller.ts](./src/app.controller.ts).


## How to start service

- configure *.env*  file.

Follow the *.env.sample* and config your variables.
The configure reading file is [config.ts](./src/config.ts).

- install dependencies

```bash
npm install
```

- build file

```bash
npm run build
```

- start service

```bash
pm2 start ecosystem
```



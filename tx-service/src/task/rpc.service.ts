import { Injectable, HttpService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class RPCService {
  constructor(private config: ConfigService, private http: HttpService) {}

  async getSwapInfo(swapId: string, type: string) {
    // https://github.com/fsn-dev/fsn-rpc-api/blob/master/fsn-rpc-api.md#fsn_getmultiswap
    // https://github.com/fsn-dev/fsn-rpc-api/blob/master/fsn-rpc-api.md#fsn_getSwap

    const TAKE_SWAP_TYPES = {
      TakeSwapFunc: 'fsn_getSwap',
      TakeMultiSwapFunc: 'fsn_getMultiSwap',
    };
    const result: any = { FromAssetID: [], ToAssetID: [] };

    if (!TAKE_SWAP_TYPES[type]) return result;

    const RPC_URL = this.config.get('rpc_url');
    const method = TAKE_SWAP_TYPES[type];
    return this.http
      .post(
        RPC_URL,
        {
          jsonrpc: '2.0',
          id: 1,
          method,
          params: [swapId, 'latest'],
        },
        { headers: { 'Content-Type': 'application/json' } },
      )
      .toPromise()
      .then(res => res.data)
      .then(data => data.result)
      .then(resultData => {
        if (!resultData) return result;
        const { FromAssetID, ToAssetID } = resultData;
        if (method === 'fsn_getSwap') {
          result.FromAssetID.push(FromAssetID);
          result.ToAssetID.push(ToAssetID);
        }
        if (method === 'fsn_getMultiSwap') {
          result.FromAssetID = FromAssetID;
          result.ToAssetID = ToAssetID;
        }
        // result.FromStartTime = resultData.FromStartTime;
        // result.FromEndTime = resultData.FromEndTime;
        // result.ToStartTime = resultData.ToStartTime;
        // result.ToEndTime = resultData.ToEndTime;
      })
      .catch(e => {
        console.log('Get swap information faile', JSON.stringify(e));
        return result;
      });
  }
}

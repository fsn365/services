export interface AddressAssetMsg {
  address: string;
  asset: string;
  timestamp: number;
}
export interface AddressMsg {
  address: string;
  miner: true;
  timestamp: number;
}

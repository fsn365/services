export class CreateAddressDto {
  address: string;
  timestamp: number;
  miner?: boolean;
  contract?: boolean;
  usan?: number | string;
}

export class UpdateAddressDto extends CreateAddressDto {}

export class UpdateAddressBalanceDto {
  address: string;
  asset: string;
  qty?: number;
  qty_in?: number;
}

interface TimeLockRecord {
  value: number;
  startTime: number;
  endTime: number;
}

export class UpdateAddressTlBalanceDto {
  address: string;
  asset: string;
  data: TimeLockRecord[];
}

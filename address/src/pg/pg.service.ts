import { Injectable, Logger } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import {
  UpdateAddressDto,
  UpdateAddressBalanceDto,
  UpdateAddressTlBalanceDto,
  CreateAddressDto,
} from '../address.model';

@Injectable()
export class PgService {
  private readonly logger = new Logger('PgService');
  constructor(@InjectKnex() private readonly knex: Knex) {}

  async createOrUpdateAddress(data: CreateAddressDto): Promise<boolean> {
    const { address, timestamp, ...addressData } = data;
    return this.knex.transaction(trx => {
      return trx
        .insert({
          hash: address,
          ...addressData,
          create_at: timestamp,
          active_at: timestamp,
        })
        .into('address')
        .then(() => true)
        .catch(e => this.updateAddress(data));
    });
  }

  private async updateAddress(data: UpdateAddressDto): Promise<boolean> {
    const { address, timestamp, ...update } = data;
    return this.knex.transaction(trx => {
      return trx
        .update({ active_at: timestamp, ...update })
        .where({ hash: address })
        .from('address')
        .then(() => true)
        .catch(e => {
          this.logger.error(`Update address error:`);
          this.logger.log(e);
          return false;
        });
    });
  }

  async createOrUpdateAddressAssetBalance(
    data: UpdateAddressBalanceDto,
  ): Promise<boolean> {
    return this.knex.transaction(trx => {
      return trx
        .insert(data)
        .into('address_assets')
        .then(() => true)
        .catch(e => {
          return this.updateAddressAssetBalance(data);
        });
    });
  }

  private async updateAddressAssetBalance(data: UpdateAddressBalanceDto) {
    const { address, asset, ...update } = data;
    return this.knex.transaction(trx => {
      return trx
        .update(update)
        .where({ address, asset })
        .from('address_assets')
        .then(() => true)
        .catch(e => {
          return false;
        });
    });
  }

  async createOrUpdateAddressTlBalance(
    data: UpdateAddressTlBalanceDto,
  ): Promise<boolean> {
    return this.knex.transaction(trx => {
      return trx
        .insert({
          ...data,
          data: JSON.stringify(data.data),
        })
        .into('address_tl_assets')
        .then(() => true)
        .catch(e => this.updateAddressTlBalance(data));
    });
  }

  private async updateAddressTlBalance(data: UpdateAddressTlBalanceDto) {
    const { address, asset, ...update } = data;
    if (update.data.length === 0) {
      return this.knex.transaction(trx => {
        return trx
          .del()
          .where({ address, asset })
          .from('address_tl_assets')
          .then(() => true)
          .catch(e => false);
      });
    }

    const jsonData = JSON.stringify(update.data);
    return this.knex.transaction(trx => {
      return trx
        .update({
          ...update,
          data: jsonData,
        })
        .where({ address, asset })
        .from('address_tl_assets')
        .then(() => true)
        .catch(e => {
          this.logger.error(`Update address timelock balance error:`);
          this.logger.log(e);
          return false;
        });
    });
  }
}

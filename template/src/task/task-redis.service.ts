import { Injectable } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';

@Injectable()
export class TaskRedisService {
  private client: any;
  constructor(private readonly redisService: RedisService) {
    this.client = this.redisService.getClient('local');
  }
}

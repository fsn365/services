import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { PgService } from 'src/pg/pg.service';
import { MongoService } from 'src/mongo/mongo.service';
import { TaskRedisService } from './task-redis.service';
import { TaskHelperService } from './task-helper.service';

@Injectable()
export class TaskService {
  private readonly logger = new Logger(TaskService.name);
  constructor(
    private readonly pg: PgService,
    private readonly mongo: MongoService,
    private readonly redis: TaskRedisService,
    private readonly helper: TaskHelperService,
  ) {}

  @Cron('3 * * * * * *')
  async handleCron() {}
}

import { Injectable } from '@nestjs/common';
import { HelperService } from './task/helper.service';
import { IAssetSnapshot } from './asset.model';

@Injectable()
export class AppService {
  constructor(private helper: HelperService) {}

  getAssetSnapshot(asset: string): Promise<IAssetSnapshot> {
    return this.helper.getAssetSnapshot(asset);
  }
}

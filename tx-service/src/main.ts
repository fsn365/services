import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { config } from './config';

const logger = new Logger('Tx Service');

async function bootstrap() {
  const CONFIG = config();
  const app = await NestFactory.createMicroservice(AppModule, CONFIG.app);

  const msg = `\n\n
=========================================================
        ${CONFIG.app.name} started...
=========================================================
    `;
  await app.listen(() => {
    logger.log(msg);
  });
}
bootstrap();

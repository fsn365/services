import { Injectable, Logger } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { CreateTokenHolderInfo } from 'src/token.model';

@Injectable()
export class PgService {
  private logger = new Logger(`Erc20 Service:PgService`);

  constructor(@InjectKnex() private readonly knex: Knex) {}

  async updateTokensHolders(data: CreateTokenHolderInfo[]): Promise<boolean> {
    const promises = data.map((item: CreateTokenHolderInfo) => {
      return this.createOrUpdateAddressTokenBalance(item);
    });
    return Promise.all(promises).then();
  }

  private async createOrUpdateAddressTokenBalance(
    data: CreateTokenHolderInfo,
  ): Promise<boolean> {
    const { table, ...tokenBalance } = data;
    const { token, holder } = tokenBalance;
    return this.knex.transaction(trx => {
      trx
        .insert(tokenBalance)
        .into(table)
        .then(() => {
          this.logger.log(`Created holder:${holder} for token:${token}.`);
          return true;
        })
        .catch(e => {
          this.logger.log(
            `Create holder:${holder} for token:${token} failed. Will try update.`,
          );
          return this.updateAddressTokenBalance(tokenBalance);
        });
    });
  }

  private async updateAddressTokenBalance(
    data: Partial<CreateTokenHolderInfo>,
  ): Promise<boolean> {
    const { table, holder, token, qty } = data;
    return this.knex.transaction(trx => {
      trx
        .update({ qty })
        .where({ holder, token })
        .from(table)
        .then(() => {
          this.logger.log(
            `Updated holder:${holder} balance for token:${token}.`,
          );
          return true;
        })
        .catch(e => {
          this.logger.log(`Update holder:${holder} for token:${token} failed.`);
          this.logger.log(JSON.stringify(e));
          return false;
        });
    });
  }

  private getTransaction() {
    return new Promise(resolve => {
      this.knex.transaction(trx => resolve(trx));
    });
  }
}

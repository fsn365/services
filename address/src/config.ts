export const config = (): any => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },
  rabbitmq: {
    urls: [process.env.rabbitmq_url],
    queue: process.env.rabbitmq_queue,
    prefetchCount: 1,
    noAck: false,
    queueOptions: {
      durable: true,
    },
  },
  redis: {
    name: 'address',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
  },
  rpc_url: process.env.rpc_url,
});

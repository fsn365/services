import { Injectable, HttpService, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from 'nestjs-redis';
import { IAssetSnapshot, IUpdateAsset } from 'src/asset.model';

@Injectable()
export class HelperService {
  private logger = new Logger('AssetService:HelperService');
  private redis: any;

  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
    private readonly redisService: RedisService,
  ) {
    this.redis = this.redisService.getClient('asset');
  }

  async updateAssetsQty(assets: IUpdateAsset[]): Promise<IUpdateAsset[]> {
    const promises = assets.map(async asset =>
      this.getAssetLatestQty(asset.hash)
        .then(data => ({ ...asset, ...data }))
        .then(asset => {
          this.cacheAssetSnapShot(asset);
          return asset;
        })
        .catch(() => asset),
    );
    return Promise.all(promises).catch(() => assets);
  }

  private async getAssetLatestQty(asset: string): Promise<{ qty?: string }> {
    return this.getAssetInfo(asset).then(asset => {
      if (asset.qty !== undefined) return { qty: asset.qty };
      return {};
    });
  }

  private async getAssetInfo(asset: string): Promise<IUpdateAsset> {
    // https://github.com/fsn-dev/fsn-rpc-api/blob/master/fsn-rpc-api.md#fsn_getasset
    const method = 'fsn_getAsset';
    const RPC_URL = this.config.get('rpc_url');
    return this.http
      .post(
        RPC_URL,
        {
          jsonrpc: '2.0',
          id: 1,
          method,
          params: [asset, 'latest'],
        },
        { headers: { 'Content-Type': 'application/json' } },
      )
      .toPromise()
      .then(res => res.data)
      .then(data => data.result)
      .then(data => {
        const { Total, Symbol, Name, Owner, Decimals } = data;
        return {
          qty: Total + '',
          name: Name,
          issuer: Owner,
          precision: Decimals,
          symbol: Symbol,
        };
      })
      .catch(e => {
        this.logger.log(`Fetch asset:${asset} information failed.`);
        this.logger.log(e);
        return {};
      });
  }

  async getPrevTrackAt(): Promise<number> {
    const key = this.getTrackAtKey();
    return this.redis
      .get(key)
      .then((bk: string) => {
        if (bk) return +bk;
        return -1;
      })
      .catch(() => -1);
  }

  setPrevTrackAt(bk: number): void {
    const key = this.getTrackAtKey();
    this.redis.set(key, bk);
  }

  // cache asset's symbol, precision for transaction decoding usage
  private cacheAssetSnapShot(asset: IUpdateAsset): void {
    const { precision, symbol, hash } = asset;
    const key = this.getAssetRedisKey(hash);
    this.redis.set(key, JSON.stringify({ precision, symbol }));
  }

  async getAssetSnapshot(hash: string): Promise<IAssetSnapshot> {
    const key = this.getAssetRedisKey(hash);
    const snapshot = await this.redis
      .get(key)
      .then((data: any) => {
        const { symbol, precision } = JSON.parse(data);
        return { symbol, precision: +precision };
      })
      .catch(() => null);
    if (snapshot) return snapshot;

    const asset = await this.getAssetInfo(hash);
    if (asset.issuer) {
      this.cacheAssetSnapShot(asset);
      return {
        symbol: asset.symbol,
        precision: asset.precision,
      };
    }
  }

  private getAssetRedisKey(asset: string): string {
    return `asset:${asset};`;
  }

  private getTrackAtKey() {
    return 'asset:track_at';
  }
}

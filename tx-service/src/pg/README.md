# Txs table in PostgreSQL

- txs table

```SQL
drop table if exists txs;
create table txs (
  id         bigserial   primary key,
  hash       char(66)    unique not null,
  timestamp  int         not null,
  block      bigint      not null,
  sender     char(42)    not null,
  receiver   char(42)    not null,
  -- comptible reason
  fee        text        not null,
  type       int         not null,
  assets     jsonb       default null,
  data       json        default null
);)
```
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { config } from './config';

const logger = new Logger('address service');

async function bootstrap() {
  const appConfig = config();
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: appConfig.rabbitmq,
  });

  await app.listen(() => {
    logger.log(`Address services started...`);
  });
}
bootstrap();

import { Injectable, BadRequestException, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IPagedBlock, BlockQueryDto, IBlock } from '../models/block.model';
import { MinerInfo } from '../models/miner.model';
import BlockDoc from './block.interface';

@Injectable()
export class MongoService {
  private logger = new Logger(`BlockService:MongoService`);

  constructor(@InjectModel('Blocks') private readonly model: Model<BlockDoc>) {}

  async aggregateMiners(
    prevTrackAt?: number,
  ): Promise<{ syncBk: number; miners: MinerInfo[] }> {
    let syncBk = prevTrackAt + 1;
    const miners = await this.model
      .aggregate([
        { $match: { number: { $gt: prevTrackAt + 1 } } },
        {
          $group: {
            _id: '$miner',
            latest: { $max: '$number' },
            timestamp: { $min: '$timestamp' },
          },
        },
      ])
      .then(stats => {
        return stats.map(({ _id, latest, timestamp }) => {
          syncBk = Math.max(latest, syncBk);
          return { address: _id, timestamp };
        });
      })
      .catch(e => {
        this.logger.log(`Aggregate miners information failed`);
        this.logger.error(e);
        return [];
      });
    return { syncBk, miners };
  }

  async getBlockByHeight(height: number): Promise<IBlock> {
    return this.model
      .findOne({ number: height }, { _id: 0 })
      .then((doc: any) => {
        if (doc) return this.cleanBlock(doc.toJSON());
        throw new BadRequestException(`Can't find a block ${height}.`);
      });
  }

  async getBlocks(lBk: number, query: BlockQueryDto): Promise<IPagedBlock[]> {
    const { page, size, order = -1 } = query;
    if (page * size > lBk) {
      throw new BadRequestException(`Queried blocks are out of range.`);
    }

    let $lte: number, $gte: number;
    if (order === 1) {
      $gte = Math.max(0, (page - 1) * size);
      $lte = Math.min(lBk, $gte + size);
    }
    if (order === -1) {
      $lte = lBk - page * size;
      $gte = Math.max(0, $lte - size);
    }
    const $match = { number: { $lte, $gte } };
    const $sort = { number: +order };

    return this.model
      .aggregate([
        { $match },
        {
          $project: {
            _id: 0,
            miner: 1,
            reward: 1,
            number: 1,
            timestamp: 1,
            txcount: 1,
          },
        },
        { $sort },
        { $limit: size },
      ])
      .then(docs => docs.map(doc => this.cleanBlock(doc)));
  }

  private cleanBlock(block: any): any {
    const { reward, txcount, number, ...others } = block;
    return {
      height: number,
      ...others,
      reward: +reward / Math.pow(10, 18),
      txs: txcount,
    };
  }
}

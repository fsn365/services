export const config = (): any => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  redis: {
    name: 'erc20',
    host: process.env.host || 'localhost',
    port: process.env.port || 6379,
  },
  erc20: {
    [process.env.erc20_btc]: {
      exchange: process.env.erc20_btc_exchange,
      symbol: 'mBTC',
      name: 'SMPC Bitcoin',
      precision: 8,
    },
    [process.env.erc20_any]: {
      exchange: process.env.erc20_any_exchange,
      symbol: 'ANY',
      name: 'Anyswap',
      precision: 18,
    },
    [process.env.erc20_eth]: {
      exchange: process.env.erc20_eth_exchange,
      name: 'Ethereum',
      symbol: 'mETH',
      precision: 18,
    },
    [process.env.erc20_usd]: {
      exchange: process.env.erc20_usd_exchange,
      name: 'Tether',
      symbol: 'mUSDT',
      precision: 6,
    },
  },
  app: {
    name: process.env.name || 'template',
    port: process.env.port || 8888,
  },
});

import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TokenHolderInfo } from '../token.model';

const Web3 = require('web3');
const web3FusionExtend = require('web3-fusion-extend');

@Injectable()
export class HelperService {
  private web3: any;
  private ERC20_TOKENS: any;

  constructor(private config: ConfigService) {
    const RPC_PROVIDER = this.config.get('rpc_url');
    this.web3 = web3FusionExtend.extend(new Web3(RPC_PROVIDER));

    const ERC20_TOKENS = this.config.get('erc20');
    if (!ERC20_TOKENS) {
      throw new InternalServerErrorException(
        `Can't find erc20 token configure.`,
      );
    }
    this.ERC20_TOKENS = ERC20_TOKENS;
  }

  // https://web3js.readthedocs.io/en/v1.2.0/web3-eth-contract.html
  // https://piyolab.github.io/playground/ethereum/getERC20TokenBalance/
  async getAddressTokenBalance(
    walletAddress: string,
    tokenContract: string,
  ): Promise<TokenHolderInfo> {
    // To Do: Ask fusion team to provide an encapsulated tool
    const qty = 1;
    return { holder: walletAddress, token: tokenContract, qty };
  }

  private getTokenInfo(tokenContract: string) {
    // To Do: ask fusion team to provide an encapuslated tool
  }
}

import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

export const SERVICE_NAME = 'BLOCK_SERVICE';

export const BLOCK_CLIENT_PROVIDER = {
  provide: SERVICE_NAME,
  inject: [ConfigService],
  useFactory: (config: ConfigService) => {
    const option = config.get('block_service');
    return ClientProxyFactory.create(option);
  },
};

import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory } from '@nestjs/microservices';

export const SERVICE_NAME = 'ADDRESS_SERVICE';

export const ADDRESS_CLIENT_PROVIDER = {
  provide: SERVICE_NAME,
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const option = configService.get('address_service');
    return ClientProxyFactory.create(option);
  },
};

import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import TxDoc from './transaction.interface';

@Injectable()
export class MongoService {
  private logger = new Logger(`Asset: MongoService`);
  constructor(
    @InjectModel('Transactions') private readonly model: Model<TxDoc>,
  ) {}

  /**
   * 1. log for GenAssetFunc type transaction:
   * "log":{
      "Description":"{\"offerID\": \"1236\"}",
      "Name":"Discount Token",
      "Symbol":"DSC1",
      "Total": 100
      "AssetID":"0xb4b5171ba461f05be33264047d58462b140fd0ef4b73feccd44fd20dc905fdb8",
      "CanChange":true,
      "Decimals":0 
   }
   * 2. log for AssetValueChangeFunc type transaction
   * "log":{
      Value: "69000000000000000000",
      "AssetID":"0x6a102b41bb79570e962309e5bc7137606a817b446326b6d0c54e28e4e498c133",
      "IsInc":false,
      "To":"0x6660fbcbd0ed302649e522517ed33450a46eb43a",
      "TransacData":"0x2f467ec6b89d3ae5fa9234f3aede4756386e7575e4bea581b2a77015a946df13"
   }
  */
  async AggregateAssets(
    prevTrackAt: number,
  ): Promise<{ syncBk: number; assets: any[] }> {
    //Transaction types: https://github.com/fsn-dev/fsn-go-sdk/tree/master/mongosync
    const types = ['GenAssetFunc', 'AssetValueChangeFunc'];
    const $match = { blockNumber: { $gt: prevTrackAt }, type: { $in: types } };
    let syncBk = prevTrackAt;

    return this.model
      .aggregate([
        { $match },
        {
          $project: {
            _id: 0,
            hash: '$log.AssetID',
            name: '$log.Name',
            symbol: '$log.Symbol',
            // qty: '$log.Total', get latest total value from rpc service
            precision: '$log.Decimals',
            issuer: '$from',
            issue_time: '$timestamp',
            issue_tx: '$hash',
            canchange: '$log.CanChange',
            description: '$log.Description',
            block: '$blockNumber',
            type: '$type',
          },
        },
      ])
      .then((assetsData: any[] = []) => {
        const assetsMap = {};
        assetsData.map(assetData => {
          const { hash, block, ...others } = assetData;
          syncBk = Math.max(block, syncBk);
          const cleanedAssetData = this.cleanAssetData(others);
          if (!assetsMap[hash]) {
            assetsMap[hash] = { ...cleanedAssetData, hash };
          } else {
            assetsMap[hash] = { ...assetsMap[hash], ...cleanedAssetData, hash };
          }
        });
        const assets = Object.keys(assetsMap).map(key => assetsMap[key]);
        return { syncBk, assets };
      })
      .catch(e => {
        this.logger.log(`Fetch assets from MongoDB error:`);
        this.logger.error(e);

        return { syncBk, assets: [] };
      });
  }

  private cleanAssetData(assetData: any) {
    const { description = '{}', type, ...asset } = assetData;

    if (type === 'AssetValueChangeFunc') {
      return {};
    }

    try {
      const desc = JSON.parse(description);
      if (Object.keys(description).length) asset.description = desc;
    } catch {
      const desc = description && description.trim();
      if (desc) {
        asset.description = { desc };
      }
    }

    return asset;
  }
}

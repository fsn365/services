import { timeStamp } from 'console';

export interface MinerInfo {
  address: string;
  timestamp: number;
}

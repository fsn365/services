# tables in PostgreSQL

- address_assets
  - address: string
  - asset: string
  - qty: number
  - qty_in: number

The primary key for *address_assets* table is (address, asset);

- address
  - id: bigserial
  - hash: string
  - miner: boolean
  - usan: text
  - contract: boolean
  - label: text
  - create_at: number
  - active_at: number
  - info: json

- address_tl_assets
  - address:string
  - asset: string
  - data: json

The primary key for *address_tl_assets* table is (address, asset);
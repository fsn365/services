import { Module } from '@nestjs/common';
import { AssetService } from './asset.service';
import { ASSET_CLIENT_PROVIDER } from './asset.client.provider';

@Module({
  providers: [ASSET_CLIENT_PROVIDER, AssetService],
  exports: [AssetService],
})
export class AssetModule {}

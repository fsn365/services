export const TRANSACTION_TYPES = {
  GenNotationFunc: 0,
  GenAssetFunc: 1,
  SendAssetFunc: 2,
  TimeLockFunc: 3,
  BuyTicketFunc: 4,
  OldAssetValueChangeFunc: 5,
  MakeSwapFunc: 6,
  RecallSwapFunc: 7,
  TakeSwapFunc: 8,
  EmptyFunc: 9,
  MakeSwapFuncExt: 10,
  TakeSwapFuncExt: 11,
  AssetValueChangeFunc: 12,
  MakeMultiSwapFunc: 13,
  RecallMultiSwapFunc: 14,
  TakeMultiSwapFunc: 15,
  ReportIllegalFunc: 16,
  Origin: 17,
  ERC20: 18,
  ANYSWAP: 19,
};

export const SWAP_TYPES = [
  'MakeSwapFunc',
  'RecallSwapFunc',
  'TakeSwapFunc',
  'MakeSwapFuncExt',
  'TakeSwapFuncExt',
  'MakeMultiSwapFunc',
  'RecallMultiSwapFunc',
  'TakeMultiSwapFunc',
];

export const FSN_TOKEN =
  '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

export const FSN_CONTRACT = '0xffffffffffffffffffffffffffffffffffffffff';

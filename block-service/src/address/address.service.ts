import { Injectable, Inject, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { from } from 'rxjs';
import { AddressAssetMsg, AddressMsg } from './msg.interface';
import { SERVICE_NAME } from './address.client.provider';
import { MinerInfo } from '../models/miner.model';
import { FSN_TOKEN } from './constants';

@Injectable()
export class AddressService {
  private logger = new Logger(`BlockService:AddressClient`);

  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  updateMinersFsnBalance(miners: MinerInfo[]): void {
    from(miners).subscribe((miner: MinerInfo) => {
      const msg: AddressAssetMsg = {
        address: miner.address,
        asset: FSN_TOKEN,
        timestamp: miner.timestamp,
      };
      this.client.emit('address:balance', msg);
      this.logger.log(`address:balance`, JSON.stringify(msg));
    });
  }

  updateAddressInfo(miners: MinerInfo[]): void {
    from(miners).subscribe((miner: MinerInfo) => {
      const msg: AddressMsg = {
        address: miner.address,
        miner: true,
        timestamp: miner.timestamp,
      };
      this.client.emit('address', msg);
      this.logger.log(`update address info`, JSON.stringify(msg));
    });
  }
}

import { Injectable, HttpService, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from 'nestjs-redis';

@Injectable()
export class RPCService {
  private logger = new Logger(`AddressService:LoggerService`);
  private readonly redis: any;

  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
    private redisService: RedisService,
  ) {
    this.redis = this.redisService.getClient('address');
  }

  // get asset balance for an address
  async getAddressAssetBalance(
    address: string,
    asset: string,
  ): Promise<{ address: string; asset: string; qty: number }> {
    const method = 'fsn_getBalance';
    const params = [asset, address];
    const [balance, assetData = { Decimals: 0 }] = await Promise.all([
      this.makeRequest(method, params),
      this.getAssetInfo(asset),
    ]);

    const qty = this.calcQty(balance, assetData);
    return { address, asset, qty };
  }

  // Get asset time lock balance for an address
  async getAddressAssetTlBalance(
    address: string,
    asset: string,
  ): Promise<{ address: string; asset: string; data: any; qty_in?: number }> {
    const method = 'fsn_getTimeLockBalance';
    const params = [asset, address];
    const [items = [], assetData] = await Promise.all([
      this.makeRequest(method, params),
      this.getAssetInfo(asset),
    ]);

    // please refer fusion document
    const FOREVER = 18446744073709552000;
    let qty_in = -1;

    const data = items.map(({ Value, StartTime, EndTime }) => {
      const value = this.calcQty(Value, assetData);
      if (EndTime === FOREVER) {
        qty_in = qty_in === -1 ? value : qty_in + value;
      }
      return {
        value,
        startTime: StartTime,
        endTime: EndTime,
      };
    });

    const balance = { data, qty_in };
    if (balance.qty_in === -1) delete balance.qty_in;

    return { address, asset, ...balance };
  }

  // Get data from fusion blockchain rpc service.
  // Fusion RPC service: https://github.com/FUSIONFoundation/efsn/wiki/FSN-RPC-API
  private async makeRequest(method: string, params: string[]) {
    this.logger.log(`${method}, ${JSON.stringify(params)}`);

    const RPC_URL = this.config.get('rpc_url');
    return this.http
      .post(RPC_URL, {
        jsonrpc: '2.0',
        id: 1,
        method,
        params: [...params, 'latest'],
      })
      .toPromise()
      .then(res => res.data)
      .then(data => data.result)
      .then(data => (data.Items ? data.Items : data))
      .catch(e => {
        this.logger.log(
          `Make request failed. $${method}, ${JSON.stringify(params)}`,
        );
        console.error(JSON.stringify(e));
        return null;
      });
  }

  // Get asset's key informaton: Decimals, Symbol
  private async getAssetInfo(asset: string) {
    const assetKey = `address_service:asset:${asset}`;
    const cachedAssetInfo = await this.redis
      .get(assetKey)
      .then((data: string) => JSON.parse(data))
      .catch(() => null);
    if (cachedAssetInfo) return cachedAssetInfo;

    const method = 'fsn_getAsset';
    const assetInfo = await this.makeRequest(method, [asset]);
    const { Symbol, Decimals } = assetInfo;
    const snapshot = { symbol: Symbol, precision: Decimals };

    this.redis.set(assetKey, JSON.stringify(snapshot));

    return snapshot;
  }

  private calcQty(
    balance: string,
    assetData: { precision: number; symbol: string },
  ): number {
    return +balance / Math.pow(10, assetData.precision);
  }
}

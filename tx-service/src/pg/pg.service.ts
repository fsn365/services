import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';

@Injectable()
export class PgService {
  constructor(@InjectKnex() private readonly knex: Knex) {}

  async insertTxsToDB(txs: any[]): Promise<number> {
    const sortedTxs = txs.sort((txA, txB) => txA.block - txB.block);
    return this.knex.transaction(trx => {
      return trx
        .insert(sortedTxs)
        .returning('id')
        .then(ids => {
          console.log(ids);
          return Math.max(ids[0], ids[ids.length - 1]);
        })
        .catch(e => -1);
    });
  }

  async findPrevTrackTx(): Promise<number> {
    return this.knex
      .select('block')
      .from('txs')
      .orderBy('id', 'desc')
      .then((record: any) => {
        if (record) return record.block;
        return -1;
      });
  }
}

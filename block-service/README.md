# Block services

## Configure

After setting up enviroment varibles at *.env* at root directory,
program reads the configure from *process.env*.

The configure reading file is [config.ts](./src/config.ts).

## Functionalities
- Track miners from *Blocks* collection 
- Send messages to *Address* service
  - updating addresses' FSN balance
  - create an address for a miner or tag an address as a miner.
- provide API services 
    - block
    - blocks
    - current block height

## How to start service

- install dependencies

```bash
npm install
```

- build file

```bash
npm run build
```

- start service

```bash
pm2 start ecosystem
```
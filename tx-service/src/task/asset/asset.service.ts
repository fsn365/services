import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './asset.client.provider';
import { FSN_TOKEN } from '../constants';

@Injectable()
export class AssetService {
  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  async getAssetSnapshot(
    assetHash: string,
  ): Promise<{ precision: number; symbol: string }> {
    if (assetHash === FSN_TOKEN) return { precision: 18, symbol: 'FSN' };

    return this.client.send('snapshot', assetHash).toPromise();
  }
}

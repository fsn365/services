import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor() {}

  async getTxDecodingProgress(): Promise<number> {
    return Promise.resolve(245000);
  }
}

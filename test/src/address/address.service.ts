import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './address.client.provider';

@Injectable()
export class AddressService {
  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  // The ClientProxy is lazy.
  async onApplicationBootstrap() {
    await this.client.connect();
    const address = {
      address: '0x53781221c8a0b40d0f7d9b52a75409dbeffda634',
      usan: 10139396,
      label: 'www.fsn365.com',
      create_at: 1586858579,
    };
    // this.client.emit('address', address);
    const msg = {
      address: '0xcb882a4deb4e376c53a1944433430eed73cc5e98',
      asset:
        '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    };
    this.client.emit('address:tl_balance', msg);
  }
}

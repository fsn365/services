export const config = () => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  redis: {
    name: 'local',
    host: 'localhost',
    port: 6379,
    ttl: 4,
  },
  rpc: process.env.rpc_url,
  app:{
    name: process.env.name || 'template',
    port: process.env.port || 8888
  }
});

interface TxData {
  value: number;
  symbol: string;
  asset: string;
  // time lock transaction
  startTime?: number;
  endTime?: number;
  type?: string;
  // asset value change transaction
  isInc?: boolean;
  usan?: number;
}

interface USANTxData {
  usan: number;
}

interface SwapTxData {
  swap: string;
}

interface ProcessedTxBase {
  hash: string;
  type: number;
  fee: string;
  sender: string;
  receiver: string;
  status: number; // 0 || 1
  //Other types of transaction and USAN generation transaction has no assets field
  timetamp: number;
}

export interface ProcessedTx {
  //Other types of transaction and USAN generation transaction has no assets field
  assets?: string[];
  data: TxData | USANTxData | SwapTxData;
  ivalue?: string;
  dvalue?: string;
  erc20Receipts?: any[];
}

export interface JsonlizedTx extends ProcessedTxBase {
  data: string;
  assets?: string;
}

interface RawTxBase {
  hash: string;
  type: string;
  gasLimit: string;
  gasPrice: string;
  satus: number;
  from: string;
  to: string;
  timestamp: number;
  log: object | any[] | null;
  ivalue: string;
  dvalue: string;
}

export interface RawTx extends Partial<RawTxBase> {}

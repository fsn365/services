import { Injectable, Logger } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { Interval } from '@nestjs/schedule';
import { MongoService } from './mongo/mongo.service';
import { ConfigService } from '@nestjs/config';
import { AddressService } from './address/address.service';
import { IPagedBlock, IBlock, BlockQueryDto } from './models/block.model';

const Web3 = require('web3');
const web3FusionExtend = require('web3-fusion-extend');

@Injectable()
export class AppService {
  private logger = new Logger(`Block:AppService`);
  private redis: any;
  private web3: any;

  constructor(
    private readonly mongo: MongoService,
    private readonly config: ConfigService,
    private readonly redisService: RedisService,
    private readonly addressService: AddressService,
  ) {
    const RPC_URL = this.config.get('rpc_url');
    this.redis = this.redisService.getClient('local');
    this.web3 = web3FusionExtend.extend(new Web3(RPC_URL));
  }

  @Interval(6000)
  private async handleInterval() {
    this.trackLatestBk();
    this.trackMiners();
  }

  getBlock(height: number): Promise<IBlock> {
    return this.mongo.getBlockByHeight(height);
  }

  async getBlocks(query: BlockQueryDto): Promise<IPagedBlock[]> {
    const lBk = await this.getLBK();
    return this.mongo.getBlocks(lBk, query);
  }

  async getLBK(): Promise<number> {
    return this.redis
      .get('l:bk')
      .then((val: string) => +val)
      .catch(e => -1);
  }

  private cacheLBk(bk: number) {
    this.redis.set('l:bk', bk);
  }

  private trackLatestBk() {
    this.web3.eth.getBlockNumber((error: any, result: number) => {
      if (error) {
        console.log(error);
      }
      if (result) {
        this.cacheLBk(result);
      }
    });
  }

  private async trackMiners() {
    const prevSyncAt = await this.getMinersSyncHeight();
    const { miners, syncBk } = await this.mongo.aggregateMiners(prevSyncAt);
    if (miners.length) {
      this.addressService.updateMinersFsnBalance(miners);
      this.addressService.updateAddressInfo(miners);
      let t1 = setTimeout(() => {
        clearTimeout(t1);
        t1 = null;
        this.setMinersSyncHeight(syncBk);
      }, 10);
    }
  }

  private getMinersSyncHeight() {
    return this.redis
      .get('bk:track_at')
      .then((val: string) => {
        if (val) return +val;
        return -1;
      })
      .catch((e: any) => {
        this.logger.log(`Get bk:track_at error:`);
        this.logger.error(e);
        return -1;
      });
  }

  private setMinersSyncHeight(height: number) {
    this.redis.set('bk:track_at', height);
  }
}
